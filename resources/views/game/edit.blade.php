@extends('layout.master')

@section('judul')
<h1> Edit Game {{$game->nama}} </h1> 
@endsection
    
@section('content') 
<form action="/game/{{$game->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label >Nama Game</label>
      <input type="string"  class="form-control" value="{{$game->nama}}" name="nama" placeholder="Masukan Nama Game">
    @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
    </div>
   
<div class="form-group">
        <label >Game Play</label>
        <textarea  name="gameplay" class="form-control" cols="30" rows="10" > {{$game->gameplay}}   </textarea>
     @error('gameplay')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
        @enderror
    </div>     

 <div class="form-group">
      <label >Developer</label>
      <input type="string"  class="form-control" value="{{$game->developer}}" name="developer" placeholder="Masukan Developer">
    @error('developer')
    <div class="alert alert-danger">
        {{ $message }}</div>
        @enderror
    </div>
  
<div class="form-group">
      <label >Tahun</label>
      <input type="integer"  class="form-control" value="{{$game->year}}" name="year" placeholder="Masukan Tahun">
    @error('year')
    <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Update</button>
  </form>

@endsection