@extends('layout.master')

@section('judul')
<h1> Halaman Cast </h1> 
@endsection
@section('judul2')
<h5> List Data Cast </h5>  
@endsection
    
@section('content') 
<a href="/game/create" class="btn btn-primary mb-3">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio Data</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
@forelse ($game as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->gameplay}}</td>
                        <td>{{$value->developer}}</td>
   <td>{{$value->year}}</td>
                        <td>
                              <form action="/game/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/game/{{$value->id}}" class="btn btn-info">Detail</a>
                                <a href="/game/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                <input type="submit" onclick="return confirm('anda yakin akan menghapus data ini ?')" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse    
            </tbody>
        </table>
    

@endsection