@extends('layout.master')

@section('judul')
<h1> Nama Game </h1> 
@endsection
    
@section('content') 
<form action="/game" method="POST">
    @csrf
    <div class="form-group">
      <label >Nama Game</label>
      <input type="string" name="nama" class="form-control" placeholder="Masukan Nama Game" >
     @error('nama')
    <div class="alert alert-danger">
      {{ $message }}
    </div>
    @enderror
    </div>

<div class="form-group">
        <label >Game Play</label>
        <textarea name="gameplay" class="form-control" cols="30" rows="10" > </textarea>
     @error('gameplay')
    <div class="alert alert-danger">
      {{ $message }}
    </div>
    @enderror
  </div>

<div class="form-group">
      <label >Developer</label>
      <input type="string" name="developer" class="form-control" placeholder="Masukan Developer" >
    @error('developer')
    <div class="alert alert-danger">
      {{ $message }}
    </div>
    @enderror
    </div>

    <div class="form-group">
      <label >Tahun</label>
      <input type="integer" name="year" class="form-control" placeholder="Masukan Tahun">
    @error('tahun')
    <div class="alert alert-danger">
      {{ $message }}
    </div>
    @enderror
  </div>
  <button type="submit" class="btn btn-primary">Tambah</button>
  </form>

@endsection