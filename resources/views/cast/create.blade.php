@extends('layout.master')

@section('judul')
<h1> Tambah Cast </h1> 
@endsection
    
@section('content') 
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label >Nama Peran</label>
      <input type="string" name="nama" class="form-control" placeholder="Masukan Nama Peran" >
    
    @error('nama')
    <div class="alert alert-danger">
      {{ $message }}
    </div>
    @enderror
    </div>

    <div class="form-group">
      <label >Umur</label>
      <input type="integer" name="umur" class="form-control" placeholder="Masukan Umur">
    @error('umur')
    <div class="alert alert-danger">
      {{ $message }}
    </div>
    @enderror
  </div>

    <div class="form-group">
        <label >Biodata</label>
        <textarea name="bio" class="form-control" cols="30" rows="10" > </textarea>
     @error('bio')
    <div class="alert alert-danger">
      {{ $message }}
    </div>
    @enderror
  </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
  </form>
@endsection
