@extends('layout.master')

@section('judul')
<h1> Edit Cast {{$cast->nama}} </h1> 
@endsection
    
@section('content') 
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label >Nama Peran</label>
      <input type="string"  class="form-control" value="{{$cast->nama}}" name="nama" placeholder="Masukan Nama Peran">
    @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
    </div>
    <div class="form-group">
      <label >Umur</label>
      <input type="integer"  class="form-control" value="{{$cast->umur}}" name="umur" placeholder="Masukan Umur">
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-group">
        <label >Biodata</label>
        <textarea  name="bio" class="form-control" cols="30" rows="10" > {{$cast->bio}}   </textarea>
      
     @error('bio')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
        @enderror
    </div>     
    <button type="submit" class="btn btn-primary">Update</button>
  </form>
@endsection
