@extends('layout.master')

@section('judul')
<h1> Halaman Update Profile </h1> 
@endsection

@push('script')
<script>
  src="htps://cdn.tiny.cloud/1/py2ujlv1t3k8n7fzfq2xm1tedm2t7snn2ooyc2if6m3wq31x/tinymce/5/tinymce.min.js" referrerpolicy="origin">
</script>

<script>
  tinymce.init({
      selector: 'textarea',
      plugins: 'advlist autolink lists link image charpmap print preview hr achor pagebreak',
      toolbar_mode: 'floating',
  });
</script>
@endpush

@section('content')    

<form action="/profil/{{$profile->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label >Nama User</label>
      <input type="text" value="{{$profile->user->name}}" class="form-control" disabled>
    </div>
    
   
    <div class="form-group">
      <label >Email User</label>
      <input type="text" value="{{$profile->user->email}}" class="form-control" disabled>
    </div>
    
    
    <div class="form-group">
      <label >Umur Profile</label>
      <input type="number"  class="form-control" value="{{$profile->umur}}" name="umur" placeholder="Masukan Umur">
    @error('umur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
    </div>

    <div class="form-group">
        <label >Bio Data</label>
        <textarea  name="bio" class="form-control">{{$profile->bio}}</textarea>
      @error('bio')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
      </div>

      <div class="form-group">
        <label >Alamat</label>
        <textarea  name="alamat"class="form-control">{{$profile->alamat}}</textarea>
      @error('alamat')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
      </div>
    
    <button type="submit" class="btn btn-primary">Update</button>
  </form>
@endsection
