@extends('layout.master')

@section('judul')
<h1> Halaman Genre </h1> 
@endsection
@section('judul2')
<h5> List Data Genre </h5>  
@endsection
    
@section('content') 
<a href="/genre/create" class="btn btn-primary mb-3">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">List Film</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($genre as $key=>$item)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$item->nama}}</td>
                        <td>  
                            <ul>
                                @foreach ($item->film as $value)
                                <li>{{$value->judul}}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td>
                            <form action="/genre/{{$item->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/genre/{{$item->id}}" class="btn btn-info">Detail</a>
                                <a href="/genre/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
                                <input type="submit" onclick="return confirm('anda yakin akan menghapus data ini ?')" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
    @endsection
