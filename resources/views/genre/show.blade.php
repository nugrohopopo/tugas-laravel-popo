@extends('layout.master')

@section('judul')
<h1> Detail Peran {{$genre->nama}}</h1> 
@endsection
@section('judul2')
<h5>Genre Detail  </h5>  
@endsection
    
@section('content') 
<h1> {{$genre->nama}}</h1> 

<div class="row">
    @foreach ($genre->film as $item)
     <div class="col-4">
        <div class="card">
            <img src="{{asset('img_film/'.$item->poster)}}" class="card-img-top" alt="...">
            <div class="card-body">
                <h3> {{$item->judul}}</h3>
                <p class="card-text">{{ $item->ringkasan,20}}</p>
              
            </div>
          </div>
    </div>
    @endforeach
</div>
 



@endsection
