@extends('layout.master')

@section('judul')
<h1> Tambah Genre </h1> 
@endsection
    
@section('content') 
<form action="/genre" method="POST">
    @csrf
    <div class="form-group">
      <label >Nama Genre</label>
      <input type="string" name="nama" class="form-control" placeholder="Masukan Nama Genre" >
    
    @error('nama')
    <div class="alert alert-danger">
      {{ $message }}
    </div>
    @enderror
    </div>
    
    <button type="submit" class="btn btn-primary">Tambah</button>
  </form>
@endsection
