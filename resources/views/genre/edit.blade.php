@extends('layout.master')

@section('judul')
<h1> Edit Genre {{$genre->nama}} </h1> 
@endsection
    
@section('content') 
<form action="/genre/{{$genre->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label >Nama Peran</label>
      <input type="string"  class="form-control" value="{{$genre->nama}}" name="nama" placeholder="Masukan Nama Peran">
    @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
    </div>
    
    <button type="submit" class="btn btn-primary">Update</button>
  </form>
@endsection
