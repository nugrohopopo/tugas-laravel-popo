<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
     <img src="{{asset('Admin/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
     <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        @guest
            
        @endguest
        @auth
        <img src="{{asset('Admin/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        @endauth
        
      </div>
      <div class="info">
        @guest
        <a href="#" class="d-block">Belum Login</a> 
        @endguest
        @auth
        <a href="#" class="d-block">{{ Auth::user()->name }} ({{Auth::user()->profile->umur}}) </a> 
        {{-- ({{Auth::user()->profile->umur}}) --}}
        @endauth
      </div>
    </div>

    <!-- SidebarSearch Form -->
    

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="/" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                     Dashboard
                </p>
            </a>
        </li>
        
        <li class="nav-item">
          <a href="/table" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Table
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/table" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Table</p>
              </a>
            </li>
        
            <li class="nav-item">
              <a href="/data-table" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Data Table</p>
              </a>
            </li>
          </ul>

          <li class="nav-item">
            <a href="/film" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>List Film</p>
            </a>
          </li>
            
          @auth
              
          
          <li class="nav-item">
            <a href="/genre" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>Genre Film</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="/profil" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>Profile</p>
            </a>
          </li>

            <li class="nav-item">
              <a class="nav-link bg-danger" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                  <i class="fa fa-sign-out" aria-hidden="true"></i>
                  <p>Logout</p>
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
            </li>

            @endauth
            
            @guest
            <li class="nav-item">
              <a href="/login " class="nav-link bg-primary">
                <i class="nav-icon fas fa-th"></i>
                <p>Login</p>
              </a>
            </li>
            @endguest
    </nav>
    <!-- /.sidebar-menu -->
  </div>