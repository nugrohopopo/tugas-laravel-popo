@extends('layout.master')

@section('judul')
<h1> Tambah Film </h1> 
@endsection

@push('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
  $(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
    
@endpush
    
@section('content') 
<form action="/film" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label >Judul Film</label>
      <input type="text" name="judul" class="form-control" placeholder="Masukan Judul Film" >
    @error('judul')
    <div class="alert alert-danger">
      {{ $message }}
    </div>
    @enderror
    </div>
    
    <div class="form-group">
      <label >Ringkasan Film</label>
      <textarea  name="ringkasan" class="form-control" placeholder="Ringkasan Film" > </textarea>
    @error('ringkasan')
    <div class="alert alert-danger">
      {{ $message }}
    </div>
    @enderror
    </div>

    <div class="form-group">
      <label >Tahun Film</label>
      <input type="string" name="tahun" class="form-control" placeholder="Masukan Tahun Film" >
    @error('tahun')
    <div class="alert alert-danger">
      {{ $message }}
    </div>
    @enderror
    </div>

    <div class="form-group">
      <label >Poster</label>
      <input type="file" name="poster" class="form-control" >
    @error('poster')
    <div class="alert alert-danger">
      {{ $message }}
    </div>
    @enderror
    </div>

    <div class="form-group">
      <label >Genre</label>
      <select  name="genre_id" class="js-example-basic-single" style="width: 100%" id="">
          <option value="">--Pilih Genre--</option>

          @foreach ($genre as $item)
            <option value="{{$item->id}}">{{$item->nama}}</option>
            @endforeach
      </select>
    @error('genre_id')
    <div class="alert alert-danger">
      {{ $message }}
    </div>
    @enderror
    </div> 


    <button type="submit" class="btn btn-primary">Tambah</button>
  </form>
@endsection
