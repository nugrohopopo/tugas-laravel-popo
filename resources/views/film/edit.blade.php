@extends('layout.master')

@section('judul')
<h1> Edit Film </h1> 
@endsection
    
@section('content') 
<form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
      <label >Judul Film</label>
      <input type="text" name="judul" value="{{$film->judul}}" class="form-control" placeholder="Masukan Judul Film" >
    @error('judul')
    <div class="alert alert-danger">
      {{ $message }}
    </div>
    @enderror
    </div>
    
    <div class="form-group">
      <label >Ringkasan Film</label>
      <textarea  name="ringkasan" class="form-control" cols="30" placeholder="Ringkasan Film" > {{$film->ringkasan}}</textarea>
    @error('ringkasan')
    <div class="alert alert-danger">
      {{ $message }}
    </div>
    @enderror
    </div>

    <div class="form-group">
      <label >Tahun Film</label>
      <input type="string" name="tahun" value="{{$film->tahun}}"class="form-control" placeholder="Masukan Tahun Film" >
    @error('tahun')
    <div class="alert alert-danger">
      {{ $message }}
    </div>
    @enderror
    </div>

    <div class="form-group">
      <label >Poster</label>
      <input type="file" name="poster" class="form-control" >
    @error('poster')
    <div class="alert alert-danger">
      {{ $message }}
    </div>
    @enderror
    </div>

    <div class="form-group">
      <label >Genre</label>
      <select  name="genre_id" class="form-control" id="">
          <option value="">--Pilih Genre--</option>

          @foreach ($genre as $item)
            @if ($item->id === $film->genre_id)
            <option value="{{$item->id}}" selected>{{$item->nama}}</option>
            @else
            <option value="{{$item->id}}">{{$item->nama}}</option>
            @endif
            @endforeach
      </select>
    @error('genre_id')
    <div class="alert alert-danger">
      {{ $message }}
    </div>
    @enderror
    </div> 


    <button type="submit" class="btn btn-primary">Update</button>
  </form>
@endsection
