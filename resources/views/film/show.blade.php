@extends('layout.master')

@section('judul')
<h1> List Film </h1> 
@endsection
    
@section('content') 
<img src="{{asset('img_film/'.$film->poster)}}" alt="">
<h4>{{$film->judul}}</h4>
<p>{{$film->ringkasan}}</p>
<p>Tahun Release : {{$film->tahun}}</p>
<p>Tgl Berita : {{$film->created_at}}</p> 

<h1>Kritik</h1>
@foreach ($film->kritik as $item)
    <div class="card">
        <div class="card-body">
            <small><b>{{$item->user->name}}</b></small>
            <p class="card-text">{{$item->isi}}</p>
            <p class="card-text">Nilai/Point= {{$item->point}}</p>
        </div>
    </div>
@endforeach
<form action="/kritik" method="POST" enctype="multipart/form-data" class="my-3">
    @csrf
    
    <div class="form-group">
      <label >Isi Kritik Dari Film</label>
      <input type="hidden" name="film_id" value="{{$film->id}}" id="">
      <textarea  name="isi" class="form-control" cols="30" rows="10" > </textarea>
    @error('isi')
    <div class="alert alert-danger">
      {{ $message }}
    </div>
    @enderror
    </div>

    <div class="form-group">
        <label >Berikan Point Anda 1-10</label>
        <input type="string" name="point" class="form-control" placeholder="berikan nilai 1-10" >
                @error('point')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
    </div>

        <button type="submit" class="btn btn-primary">Tambah</button>
  </form>

 <a href="/film" class="btn btn-secondary">Kembali</a>
@endsection