@extends('layout.master')

@section('judul')
<h1> List Film </h1> 
@endsection
    
@section('content') 
@auth
<a href="/film/create" class="btn btn-primary my-2"> Tambah Film</a>  
@endauth
<div class="row">
    @forelse ($film as $item)
    <div class="col-4">
        <div class="card">
            <img src="{{asset('img_film/'.$item->poster)}}" class="card-img-top" alt="...">
            <div class="card-body">
              <span class="badge badge-info">{{$item->genre->nama}}</span>
              <h3> {{$item->judul}}</h3>
              <p class="card-text">{{ Str::limit($item->ringkasan,20)}}</p>
              @auth
              <form action="/film/{{$item->id}}" method= "POST">
                @csrf
                @method('Delete')
                <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>  
                <a href="/film/{{$item->id}}/edit" class="btn btn-info btn-sm">Edit</a>
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
              </form>  
              @endauth
              @guest
              <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>  
              @endguest
              
            </div>
          </div>


</div>
        
    @empty
        <h1>Data Masih Kosong<h1
    @endforelse

@endsection