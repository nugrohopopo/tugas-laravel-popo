<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 Route::get('/ ', 'DashboardController@index');

Route::get('/form', 'FormController@bio');

Route::post('/kirim', 'FormController@kirim');

Route::get('/data-table', function(){
        return view('table.data-table');
    });

Route::get('/table', function(){
    return view('table.table');
});

Route::group(['middleware' => ['auth']], function () {
//CRUD Genre query builder
Route::get('/genre/create', 'GenreController@create');
Route::post('/genre', 'GenreController@store');
Route::get('/genre', 'GenreController@index');
Route::get('/genre/{genre_id} ', 'GenreController@show');
Route::get('/genre/{genre_id}/edit ', 'GenreController@edit');
Route::put('/genre/{genre_id} ', 'GenreController@update');
Route::delete('/genre/{genre_id} ', 'GenreController@destroy');

//update profile
Route::resource('profil', 'ProfileController')->only([
    'index','update'
]);
//Kritik
Route::resource('kritik', 'KritikController')->only([
    'index','store'
]);

});




//CRUD Film ORM
Route::resource('film', 'FilmController');

//Auth
Auth::routes();


// CRUD Katagori query builder
// Route::get('/cast/create ', 'CastController@create');
// Route::post('/cast ', 'CastController@store');
// Route::get('/cast ', 'CastController@index');
// Route::get('/cast/{cast_id} ', 'CastController@show');
// Route::get('/cast/{cast_id}/edit ', 'CastController@edit');
// Route::put('/cast/{cast_id} ', 'CastController@update');
// Route::delete('/cast/{cast_id} ', 'CastController@destroy');

// Route::get('/ ', 'GameController@index');
// Route::get('/game/create ', 'GameController@create');
// Route::post('/game ', 'GameController@store');
// Route::get('/game ', 'GameController@index');
// Route::get('/game/{game_id} ', 'GameController@show');
// Route::get('/game/{game_id}/edit ', 'GameController@edit');
// Route::put('/game/{game_id} ', 'GameController@update');
// Route::delete('/game/{game_id} ', 'GameController@destroy');



    
// Route::get('/ ', 'IndexController@index');

// Route::get('/register', 'AuthController@bio');

// Route::post('/kirim', 'AuthController@kirim');

// Route::get('/master', function(){
//     return view('layout.master');
// });


// Route::get('/home', 'HomeController@index')->name('home');
