<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class GameController extends Controller
{
    public function create(){
        return view('game.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ]);

        DB::table('game')->insert([
            'nama' => $request['nama'],
            'gameplay' => $request['gameplay'],
            'developer' => $request['developer'],
            'year' => $request['year']
        ]);
        return redirect('/game');
    } 
    public function index(){
        $game = DB::table('game')->get(); 
        return view('game.index', compact('game'));
    }
    public function show($id){
        $game = DB::table('game')-> where('id', $id)->first();
        return view('game.show', compact('game'));

    }
    public function edit($id){
        $game = DB::table('game')-> where('id', $id)->first();
        return view('game.edit', compact('game'));

    }
    
    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ]);

        $effect = DB::table('game')
        ->where('id', $id)
        ->update(
                [
                'nama' => $request['nama'],
                'gameplay' => $request['gameplay'],
     'developer' => $request['developer'],
                'year' => $request['year']
                ]
            );
            return redirect('/game');   
    } 

    public function destroy($id){
        DB::table('game')->where('id',$id)->delete();
        return redirect('/game');
    }

}
