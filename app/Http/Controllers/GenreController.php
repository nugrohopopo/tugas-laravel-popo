<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Genre;
use RealRashid\SweetAlert\Facades\Alert;

class GenreController extends Controller
{
    public function create(){
        return view('genre.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required'
        ]);

        DB::table('genre')->insert([
            'nama' => $request['nama']
        ]);
        Alert::success('Berhasil', 'Tambah Data Berhasil');
        return redirect('/genre');
        
    } 
    public function index(){
        //qbuilder change model> $genre = DB::table('genre')->get(); 
        $genre = Genre::all(); 
        return view('genre.index', compact('genre'));
    }
    public function show($id){
        // query Builder ganti eloquent> $genre = DB::table('genre')-> where('id', $id)->first();
        $genre = Genre::find($id);
        return view('genre.show', compact('genre'));

    }
    public function edit($id){
        $genre = DB::table('genre')-> where('id', $id)->first();
        return view('genre.edit', compact('genre'));

    }
    
    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required'
        ]);

        $effect = DB::table('genre')
        ->where('id', $id)
        ->update(
                [
                'nama' => $request['nama']
                ]
            );
            Alert::success('Berhasil', 'Edit Data Berhasil');
            return redirect('/genre');   
    } 

    public function destroy($id){
        DB::table('genre')->where('id',$id)->delete();
        Alert::success('Berhasil', ' Data Berhasil Dihapus');
        return redirect('/genre');
    }

}

